-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 02:54 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nesoul_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pollanswers`
--

CREATE TABLE `tbl_pollanswers` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `poll_answer` varchar(10) NOT NULL,
  `eventname` varchar(50) NOT NULL,
  `poll_at` datetime NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_polls`
--

CREATE TABLE `tbl_polls` (
  `id` int(11) NOT NULL,
  `poll_question` varchar(500) NOT NULL,
  `poll_opt1` varchar(500) NOT NULL,
  `poll_opt2` varchar(500) NOT NULL,
  `poll_opt3` varchar(500) NOT NULL,
  `poll_opt4` varchar(500) NOT NULL,
  `correct_ans` varchar(10) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `poll_over` int(11) NOT NULL DEFAULT '0',
  `show_results` int(11) NOT NULL DEFAULT '0',
  `eventname` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_emailid` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_team_chat`
--

CREATE TABLE `tbl_team_chat` (
  `id` int(11) NOT NULL,
  `user_id_from` varchar(20) NOT NULL,
  `user_id_to` varchar(10) NOT NULL,
  `message` varchar(500) NOT NULL,
  `chat_time` datetime NOT NULL,
  `read_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_location` varchar(500) NOT NULL,
  `user_emailid` varchar(500) NOT NULL,
  `user_mobile` varchar(15) NOT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_location`, `user_emailid`, `user_mobile`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Mahesh', 'Bengaluru', 'mahesh@neosoulsoft.com', '7019517775', '2021-01-29 16:55:23', '2021-03-19 14:54:34', '2021-03-19 14:59:04', 1, 'nesoul_demo01'),
(2, 'sham', 'Bangalore', 'sham@neosoulsoft.com', '9880328392', '2021-01-29 17:01:31', '2021-02-05 15:42:22', '2021-02-05 17:06:01', 1, 'nesoul_demo01'),
(3, 'rakshith shetty', 'india', 'rakshith@neosoulsoft.com', '9591539386', '2021-02-01 10:19:05', '2021-03-29 14:17:13', '2021-03-29 18:57:22', 1, 'nesoul_demo01'),
(4, 'Reynold', 'Bangalore', 'reynold@coact.co.in', '09611518064', '2021-02-05 15:42:45', '2021-02-10 12:53:05', '2021-02-10 12:53:25', 0, 'nesoul_demo01'),
(5, 'Harsha', 'Bangalore', 'harshith@neosoulsoft.com', '9535507992', '2021-02-09 10:29:16', '2021-02-10 13:04:05', '2021-02-10 14:17:55', 1, 'nesoul_demo01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_team_chat`
--
ALTER TABLE `tbl_team_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_team_chat`
--
ALTER TABLE `tbl_team_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
