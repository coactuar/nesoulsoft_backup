<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="background">
<div class="container-fluid">
    <div class="row content">
    
        <div class="col-12 col-md-6 col-lg-4 offset-lg-2 ">
            <form id="login-form" method="post" role="form">
            <h6>Login</h6>
                  <div id="login-message"></div>
                  
                  <div class="input-group">
                    <input type="email" class="form-control" placeholder="Email ID" aria-label="Email ID" aria-describedby="basic-addon1" name="emailid" id="emailid" required>
                  </div>
                  
                  <div class="input-group">
                    <button id="login" class="btn btn-primary btn-sm login-button" type="submit">Login</button>
                  </div>
                <div style="color:yellow">
  		Not yet a member? <a href="register.php">Rigister Here!!!!!!!</a>
  	</div>
            </form>     
	  
        </div>
	
    </div>
</div>   

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#register-form', function()
{  
  $.post('chkforregister.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        $('#register-message').text('Registered successfully');
        $('#register-message').removeClass().addClass('alert alert-success').fadeIn().delay(3000).fadeOut();
        $('#register-form').trigger("reset");
        return false;  
      }
      else if (data == '-1')
      {
          $('#register-message').text('You are already registered.');
          $('#register-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#register-message').text(data);
          $('#register-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script>
</div>
 
</body>

</html>