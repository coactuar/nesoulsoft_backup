<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="background">
<div class="container-fluid">
    <div class="row content">
        <div class="col-12 col-md-6 col-lg-4 offset-lg-1">
            <form id="register-form" method="post" role="form">
            <h6>Register</h6>
                  <div id="register-message">

                  </div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
                  </div>
                  
                  <div class="input-group">
                    <input type="email" class="form-control" placeholder="Email ID" aria-label="Email ID" aria-describedby="basic-addon1" name="emailid" id="emailid" required>
                  </div>
                  <div class="input-group">
                    <input type="number" class="form-control" placeholder="Mobile No." aria-label="Mobile No." aria-describedby="basic-addon1" name="mobile" id="mobile" required>
                  </div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Location" aria-label="Location" aria-describedby="basic-addon1" name="location" id="location" required>
                  </div>
                  
                  <div class="input-group">
                    <button id="login" class="btn btn-primary btn-sm login-button" type="submit">Register</button>
                  </div>
                 <div style="color:yellow">
  		Already a member? <a href="index.php">Sign in</a>  	</div>

            </form>        
        </div>
     
    </div>
</div>   

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#register-form', function()
{  
  $.post('chkforregister.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        $('#register-message').text('Registered successfully');
        $('#register-message').removeClass().addClass('alert alert-success').fadeIn().delay(3000).fadeOut();
        $('#register-form').trigger("reset");
        return false;  
      }
      else if (data == '-1')
      {
          $('#register-message').text('You are already registered.');
          $('#register-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#register-message').text(data);
          $('#register-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});

</script>
</div>
</body>

</html>