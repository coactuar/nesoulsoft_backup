<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';


require_once "config.php";

    $user_emailid       = mysqli_real_escape_string($link, $_POST['emailid']);
    $user_name          = mysqli_real_escape_string($link, $_POST['name']);
    $user_loc           = mysqli_real_escape_string($link, $_POST['location']);
    $user_mob           = mysqli_real_escape_string($link, $_POST['mobile']);
    
    if((trim($user_emailid) == '') || (trim($user_name)=='') || (trim($user_loc)=='') || (trim($user_mob)==''))
    {
        echo 'Please enter all details';
    }
    else{
        $query="select * from tbl_users where user_emailid ='$user_emailid' and eventname='$event_name'";
        $res = mysqli_query($link, $query) or die(mysqli_error($link)); 
        //echo $query;
        if (mysqli_affected_rows($link) > 0) 
        {
            echo "-1";
            /*$row = mysqli_fetch_assoc($res); 
                
            $login_date   = date('Y/m/d H:i:s');
            $logout_date   = date('Y/m/d H:i:s', time() + 30);
            
            try{
                $today=date("Y/m/d H:i:s");
        
                $dateTimestamp1 = strtotime($row["logout_date"]);
                $dateTimestamp2 = strtotime($today);
                //echo $row[5];
                if ($dateTimestamp1 > $dateTimestamp2)
                {
                  echo "-1";
                }                else
                {
                  $login_date   = date('Y/m/d H:i:s');
                  $logout_date   = date('Y/m/d H:i:s', time() + 30);
    
                  $query="UPDATE tbl_users set user_name='$user_name', user_location='$user_loc', user_mobile='$user_mob', login_date='$login_date', logout_date='$logout_date', logout_status='1' where user_emailid='$user_emailid' and eventname='$event_name'";
                  $res = mysqli_query($link, $query) or die(mysqli_error($link));
                  
                  $_SESSION['user_emailid']    = $user_emailid;
                  $_SESSION['user_name']     = $user_name;
                  $_SESSION["user_id"]  = $row["id"];
                  
                  echo "s";
                }
                        
       
                
            }
            catch(PDOException $e){
              echo $e->getMessage();
            }
            */
                
            
        }
        else{
            //echo "0";
            $login_date   = date('Y/m/d H:i:s');
            $logout_date   = date('Y/m/d H:i:s', time() + 30);
            $query="insert into tbl_users(user_name, user_emailid, login_date, logout_date, joining_date, logout_status, eventname, user_location, user_mobile) values('$user_name','$user_emailid','$login_date','$logout_date','$login_date','1','$event_name','$user_loc','$user_mob')";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
            $reg_id = mysqli_insert_id($link);
            if($reg_id > 0 )
            {
                  //send email
                  $mail = new PHPMailer(true);
          
                  try {
                      //Server settings
                      //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
                      $mail->isSMTP();                                            // Send using SMTP
                      $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                      $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                      $mail->Username   = 'support@coact.co.in';                     // SMTP username
                      $mail->Password   = 'coact2020';                               // SMTP password
                      $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; //STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                      $mail->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                  
                      //Recipients
                      $mail->setFrom('support@coact.co.in', 'Team COACT');
                      $mail->addAddress($user_emailid, $user_name);     // Add a recipient
                  
                      // Content
                      $mail->isHTML(true);                                  // Set email format to HTML
                      $mail->Subject = 'Thank you for registration!';
                      
                      $message ='Dear '.$user_name.',<br><br>
                      Thank you for registering for the COACT webinar.<br> 
                      Link to join: https://coact.live/demo/polls<br>
                      We look forward to your participation!  <br>
                      <br>
                       <br>
                      
                      Thanks,
                      <br>
                      Team COACT';
                      
                      
                      $mail->Body    = $message;
                  
                      $mail->send();
                      
                  } 
                  catch (Exception $e) {
                      //echo 'Mailer Error: ' . $mail->ErrorInfo;
                  }
                  
                    
                  /*$_SESSION['user_emailid']    = $user_emailid;
                  $_SESSION['user_name']     = $user_name;
                  $_SESSION["user_id"]  = $reg_id;*/
                  echo "s";
                  
                  //$_SESSION['succmsg'] = 'Registered successfully';
                  
            }
            else{
                echo "Please try again.";
            }
        }
        
    }

?>